﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VisionVolume
{


    public GameObject visionVolumeObject;
    public float detectionDelayTime = 3f;
    public List<string> tagsToDetect;


}
