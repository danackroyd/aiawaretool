﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AICharacterController : MonoBehaviour
{
    public enum AIDetectionState
    {
        undetected,
        detecting,
        detected,
        lostDetection
    }

    private AIDetectionState m_AIState;
    public float detectionCooldown = 3f;
    private float detectionCooldownTimer = 0f;
    private float detectionPreviousTimer = 0f;
    public string[] defaultTagList;
    //[SerializeField]
    public List<VisionVolume> visionVolumeObjects;
    [HideInInspector]
    public List<VisionVolumeController> visionVolumeList;


    // Use this for initialization
    void Start()
    {
        m_AIState = AIDetectionState.undetected;
        detectionCooldownTimer = detectionCooldown;

        //Setup the attached objects data and components.
        for (int i = 0; i < visionVolumeObjects.Count; i++)
        {
            if (visionVolumeObjects[i].visionVolumeObject.GetComponent<VisionVolumeController>() == null)
            {
                visionVolumeList.Add(visionVolumeObjects[i].visionVolumeObject.AddComponent<VisionVolumeController>());
                visionVolumeList[i].visionVolumeData = visionVolumeObjects[i];
                visionVolumeList[i].AICharacterController = this;
            }
            else
            {
                Debug.LogError("Vision Volume object element " + i + " listed more than once." +
                    "\nAI character Controller on object: " + gameObject);
            }

            //populate tag array
            foreach (string stringTag in defaultTagList)
            {
                visionVolumeObjects[i].tagsToDetect.Add(stringTag);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        CheckStates();
        Debug.Log("AI dectection state: " + m_AIState);
        if(m_AIState == AIDetectionState.undetected || m_AIState == AIDetectionState.lostDetection)
        {
            detectionCooldownTimer += Time.deltaTime;
        }
    }

    /// <summary>
    /// Use this function to cycle through all trigger volumes states, return first if detected state, else return if undetected state.
    /// </summary>
    private void CheckStates()
    {
        //First checks all if detected.
        for (int i = 0; i < visionVolumeList.Count; i++)
        {
            if (visionVolumeList[i].m_detectionState == VisionVolumeController.DetectionState.detected)
            {
                m_AIState = AIDetectionState.detected;
                detectionCooldownTimer = 0f;
                return;
            }
        }
        //then checks all if detecting if none detected.
        for (int i = 0; i < visionVolumeList.Count; i++)
        {
            if (visionVolumeList[i].m_detectionState == VisionVolumeController.DetectionState.detecting)
            {

                if(detectionCooldownTimer < detectionCooldown)
                {
                    m_AIState = AIDetectionState.detected;
                    return;
                }
                m_AIState = AIDetectionState.detecting;
                return;
            }
        }
        for (int i = 0; i < visionVolumeList.Count; i++)
        {
            if (visionVolumeList[i].m_detectionState == VisionVolumeController.DetectionState.undetected)
            {
                if (detectionCooldownTimer < detectionCooldown)
                {
                    m_AIState = AIDetectionState.lostDetection;
                    return;
                }
                m_AIState = AIDetectionState.undetected;
                detectionPreviousTimer = 0f;
                return;
            }
        }

    }

    public void TargetDetected()
    {
        //Debug.Log("Caught");
    }

    public void TargetDetecting(VisionVolumeController triggerVolume)
    {
        //Debug.Log("Detecting");
        if(detectionPreviousTimer != 0 && detectionPreviousTimer < triggerVolume.visionVolumeData.detectionDelayTime)
        {
            triggerVolume.detectionTimer = triggerVolume.visionVolumeData.detectionDelayTime - detectionPreviousTimer;
        }
    }

    public void TargetLost(float timeLeftBeforeDetection)
    {
        //Debug.Log(target);
        //Debug.Log(timeLeftBeforeDetection);
        detectionPreviousTimer = timeLeftBeforeDetection;
    }

    public bool CanSeeTarget(GameObject target)
    {
        //Vector3[] vertices = target.GetComponent<MeshFilter>().mesh.vertices;
        //MeshFilter[] meshes = target.GetComponentsInChildren<MeshFilter>();
        SkinnedMeshRenderer[] skinnedMeshes = target.GetComponentsInChildren<SkinnedMeshRenderer>();
        List<Vector3> vertices = new List<Vector3>();
        for (int i = 0; i < skinnedMeshes.Length; i++)
        {
            vertices.AddRange(skinnedMeshes[i].sharedMesh.vertices);
        }
        int num = Random.Range(0, vertices.Count);
        RaycastHit hit;
        //Debug.Log(vertices[num]);
        if (Physics.Raycast(transform.position, target.transform.position, out hit, Mathf.Infinity))
        {
            Debug.DrawLine(transform.position, target.transform.position, Color.red, 2f);
            Debug.Log("Did Hit");
        }
        //Debug.Log(vertices.Count);

        return true;
    }

}
