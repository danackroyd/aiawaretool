﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionVolumeController : MonoBehaviour
{
    [HideInInspector]
    public float detectionTimer;

    public VisionVolume visionVolumeData;
    public AICharacterController AICharacterController;

    private Collider m_Collider;
    private GameObject target;

    public enum DetectionState
    {
        undetected,
        detecting,
        detected
    }
    public DetectionState m_detectionState;

    //public List<GameObject> targetList;

    private void Start()
    {
        if (GetComponent<Collider>() != null)
        {            
            m_Collider = GetComponent<Collider>();
            m_Collider.isTrigger = true;
        }
        else
        {
            Debug.LogError("There is no collider on attached object: " + gameObject);
        }
        m_detectionState = DetectionState.undetected;
    }

    private void Update()
    {
        //Debug.Log(m_detectionState);
        if (m_detectionState == DetectionState.detecting)
        {
            detectionTimer += Time.deltaTime;
            if (detectionTimer > visionVolumeData.detectionDelayTime)
            {
                m_detectionState = DetectionState.detected;
                AICharacterController.CanSeeTarget(target);
            }
        }
        if(m_detectionState == DetectionState.undetected)
        {
            //Functionality moved to the AI controller.
            //if(detectionTimer > 0)
            //{
            //    detectionTimer -= Time.deltaTime;
            //}
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < visionVolumeData.tagsToDetect.Count; i++)
        {
            if (other.tag == visionVolumeData.tagsToDetect[i])
            {
                //Debug.Log("Detected " + visionVolumeData.tagsToDetect[i]);
                m_detectionState = DetectionState.detecting;
                target = other.gameObject;
                AICharacterController.TargetDetecting(this);
                AICharacterController.CanSeeTarget(target);

            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == target.tag)
        {
            m_detectionState = DetectionState.undetected;
            float timeLeftBeforeDetection = visionVolumeData.detectionDelayTime - detectionTimer;
            AICharacterController.TargetLost(timeLeftBeforeDetection);
            detectionTimer = 0f;
        }
    }

}
